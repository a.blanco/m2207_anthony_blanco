package TP4;

public class TestPokemon {
	public static void main(String[] args) {
		Pokemon p1 = new Pokemon("Pikatruc"); // Declaration et Instanciation des pokemons
		p1.sePresenter();
		
		Pokemon p2 = new Pokemon("Pikamachin");
		p2.sePresenter();
		
		int cycle = 0;
		while(p2.isAlive()) { // Tant que le pokemon 2 est en vie on le fait vivre et manger
			p2.manger();
			p2.vivre();
			cycle++;
		}
		
		// On affiche le nombre de cycle vecu
		System.out.println(p2.getNom() + " a vecu " + cycle + " cycles !");
		p2.sePresenter();
	}
	
}

	/*
	 * Resultat  1:
	 * 		Je suis Pikamachin, j'ai 61 points d'energie ( 78 Max )
	 *		Pikamachin a vecu 6 cycles !
	 * 
	 * Resultat  2:
	 * 		Je suis Pikamachin, j'ai 81 points d'energie ( 90 Max )
	 *		Pikamachin a vecu 4 cycles !
	 * 
	 * Resultat  3:
	 * 		Je suis Pikamachin, j'ai 52 points d'energie ( 55 Max )
	 *		Pikamachin a vecu 2 cycles !
	 * 
	 */