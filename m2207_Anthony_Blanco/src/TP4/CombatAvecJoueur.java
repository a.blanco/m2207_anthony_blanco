package TP4;

import java.util.Scanner;

public class CombatAvecJoueur {
	public static void main(String[] args) {
		Pokemon p1, p2;  // Declaration des variables
		int rounds = 0;
		int joueur = 0;
		
		String choix_joueur;
		String Pokemon_1;
		String Pokemon_2;
		
		Scanner sc = new Scanner(System.in); // Instanciation des objets de type scanner
		
		System.out.print("Saisissez le nom du premier combattant : "); // Saisie des nom des deux pokemons
		Pokemon_1 = sc.nextLine();
		p1 = new Pokemon(Pokemon_1); 
		
		System.out.print("Saisissez le nom du premier combattant : ");
		Pokemon_2 = sc.nextLine();
		p2 = new Pokemon(Pokemon_2); 
		
		System.out.println("");
		
		System.out.println("---------------");
		while (p1.isAlive() && p2.isAlive()) {
			
			// Recapitulatif de l'energie et de l'attaque des deux pokemons
			rounds++;
			System.out.print("Round " + rounds + "\n");
			System.out.print("Etat des combattants : " + p1.getNom() + " (en)=" + p1.getEnergie() + " (atk)=" + p1.getPuissance() + " || " + p2.getNom() + " (en)=" + p2.getEnergie() + " (atk)=" + p2.getPuissance());
			
			if (joueur == 0) { // On alterne entre les deux joueurs grace a la variable "joueur"
				System.out.println("");
				System.out.println(p1.getNom() + " : Attaquer ou Manger [A / M]");
				choix_joueur = sc.nextLine(); // On recupere la saisie du joueur
				switch (choix_joueur) {
				case ("A"): // Si la saisie = "A", on fait attaquer l'autre pokemon
					p1.attaquer(p2);
					joueur = 1;
					break;
				case ("M"): // Si la saisie = "B", on fait manger le pokemon
					p1.manger();
					joueur = 1;
					break;
				}
			} else { // Meme chose pour le joueur 2
				System.out.println("");
				System.out.println(p2.getNom() + " : Attaquer ou Manger [A / M]");
				choix_joueur = sc.nextLine();
				switch (choix_joueur) {
				case ("A"):
					p2.attaquer(p1);
					joueur = 0;
					break;
				case ("M"):
					p2.manger();
					joueur = 0;
					break;
				}
			}
		}
		System.out.println("---------------");
		
		// Quand un des deux pokemons meurt c'est la fin du combat et on affiche le gagnant, et combien de round le combat a dur�e
		if (p1.getEnergie() > p2.getEnergie()) {
			System.out.println(p1.getNom() + " gagne en " + rounds + " rounds");
		} else {
			System.out.println(p2.getNom() + " gagne en " + rounds + " rounds");
		}
		
	}
}
