package TP4;

public class Pokemon {
	// Attributs
	private int energie;
	private int energieMax;
	private String nom;
	private int puissance;
	
	//Constructeurs
	public Pokemon(String nom){
		this.nom = nom;
		this.energieMax = 50 + (int)(Math.random() * ((90 - 50) + 1));
		this.energie = 50 + (int)(Math.random() * ((this.energieMax - 50) + 1));
		this.puissance = 3 + (int)(Math.random() * ((10 - 3) + 1));
	}
	
	//M�thodes
	public void sePresenter() {
		System.out.println("Je suis " + this.nom + ", j'ai " + this.energie + " points d'energie ( " + this.energieMax + " Max ). Ma puissance et de " + this.puissance);
	}
	public void manger() {
		if (this.energie > 0) { // Si l'energie est superieur a 0 on choisie un nombre aleatoire compris entre 10 et 30
			
			int nourriture = 10 + (int)(Math.random() * ((30 - 10) + 1));
			
			if ((this.energie + nourriture) <= energieMax) { // Si la la somme de l'energie actuelle + la nourriture et plus petit ou egale a l'energieMax
				this.energie += nourriture; // On ajoute la nourriture a l'energie, sinon on jette tous
			}
		}
	}
	public void vivre() {
		this.energie -= (20 + (int)(Math.random() * ((40 - 20) + 1))); // On enleve un nobre aleatoire compris entre 20 et 40 a l'energie actuelle
		if (this.energie < 0) { // si l'energie est inferieur a 0 alors l'energie est egale a 0
			this.energie = 0;
		}
	}
	public boolean isAlive() {
		if (this.energie <= 0) { // si l'energie est inferieur a 0 alors l'energie est egale a 0 et on retourne false
			this.energie = 0;
			return (false);
		} else {
			return (true);	// sinon on retourne true
		}
	}
	public void perdreEnergie(int perte) {
		double furie = 0.25 * this.energieMax; // on calcule 25% de l'energie max
		
		if ((this.energie-perte) < 0) { // si l'energie est inferieur a 0 alors l'energie est egale a 0
			this.energie = 0;
		} else {
			if (furie < this.energie) { // si la variable furie est plus petit que l'energie, le pokemon est pas en furie.
				this.energie -= perte;
			} else { // Sionon l'energie perdue est 1.5 fois plus eleve
				this.energie -= (1.5 * perte);
			}
		}
	}
	public void attaquer(Pokemon adversaire) {
		int fatigue = (0 + (int)(Math.random() * ((1 - 0) + 1))); // Choisie un nombre aleatoire entre 0 et 1
		double furie = 0.25 * this.energieMax; // on calcule 25% de l'energie max
		
		if (furie > this.energie) { // Si le pokemon est en furie
			this.puissance *= 2; // Sa puissance est doubl�
			adversaire.perdreEnergie(this.getPuissance());
		} else {
			adversaire.perdreEnergie(this.getPuissance());
		}
		
		this.puissance -= fatigue; // Soustrait la valeur de la fatigue a la puissance
		if (this.puissance < 1) { // Si la puissance est plus petite que 1 on remet la puissance a 1 afin de ne jamais etre en dessous.
			this.puissance = 1;
		}
	}
	
	//Accesseurs
	/*Set*/
	
	
	/*Get*/
	public String getNom() {
		return (this.nom);
	}
	public int getEnergie() {
		return (this.energie);
	}
	public int getPuissance() {
		return (this.puissance);
	}
	
}
