package TP4;

public class Combat {
	public static void main(String[] args) {
		Pokemon p1, p2; // Declaration des variables
		int rounds = 0;
		
		p1 = new Pokemon("PikaMum"); // Instanciation des pokemons
		p2 = new Pokemon("PikaDady");
		
		p1.sePresenter(); // Presentation des pokemons
		p2.sePresenter();
		
		System.out.println("---------------");
		while (p1.isAlive() && p2.isAlive()) { // Tant qu'un des deux pokemon sont en vie on continue les rounds
			rounds++;
			System.out.print("Round " + rounds + " | ");
			p1.attaquer(p2); // Pokemon 1 attaque le pokemon 2 et inversement
			p2.attaquer(p1);
			
			// Recapitulatif de l'energie et de la puissance des deux pokemons
			System.out.println(p1.getNom() + " : (en) =" + p1.getEnergie() + " : (atk) =" + p1.getPuissance() + " // " + p2.getNom() + " : (en) =" + p2.getEnergie() + " : (atk) =" + p2.getPuissance());
		}
		System.out.println("---------------");
		if (p1.isAlive()) { // On verifie si le pokemon 1 est en vie, si il est en vie, il est le gagnant sinon c'est le pokemon 2 qui gagne
			System.out.println(p1.getNom() + " gagne en " + rounds + " rounds");
		} else {
			System.out.println(p2.getNom() + " gagne en " + rounds + " rounds");
		}
		
	}
}
