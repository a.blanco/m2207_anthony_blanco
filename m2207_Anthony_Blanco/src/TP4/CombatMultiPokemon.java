package TP4;

import java.util.Scanner; 

public class CombatMultiPokemon {
	public static void main(String[] args) {
		Pokemon p1_1, p1_2, p2_1, p2_2;		// Tous les pokemons
		int rounds = 0, joueur = 0, numPokemonJ1 = 0, numPokemonJ2 = 0;
		String nomPokemon;
		
		Scanner sc = new Scanner(System.in);	// Instanciation de l'objet scanner pour permettre la saisie
		
		System.out.println("--------Joueur 1---------");	//Nom des pokemons du joueur 1
		System.out.print("Saisissez le nom du pokemon n� 1 : ");
		nomPokemon = sc.nextLine(); // Saisie du pokemon 1
		p1_1 = new Pokemon(nomPokemon); // instanciation des pokemons
		System.out.print("Saisissez le nom du pokemon n� 2 : ");
		nomPokemon = sc.nextLine(); // Saisie du pokemon 2
		p1_2 = new Pokemon(nomPokemon); 
		
		System.out.println("\n--------Joueur 2---------");	//Nom des pokemons du joueur 2
		System.out.print("Saisissez le nom du pokemon n� 1 : ");
		nomPokemon = sc.nextLine(); // Saisie du pokemon 1
		p2_1 = new Pokemon(nomPokemon);
		System.out.print("Saisissez le nom du pokemon n� 2 : ");
		nomPokemon = sc.nextLine(); // Saisie du pokemon 2
		p2_2 = new Pokemon(nomPokemon);
		
		System.out.println("");
		
		while ((p1_1.isAlive() || p1_2.isAlive()) && (p2_1.isAlive() || p2_2.isAlive())) { //Tant qu'un pokemon dans chaques team est encore en vie, on continue le combat
			
			rounds++;
			System.out.print("********Round " + rounds + "*********\n"); // Affichage de l'etat des pokemons
			System.out.println("Etat des combattants de Joueur 1 : \n- " + p1_1.getNom() + " (en)=" + p1_1.getEnergie() + " (atk)=" + p1_1.getPuissance() + "\n- " + p1_2.getNom() + " (en)=" + p1_2.getEnergie() + " (atk)=" + p1_2.getPuissance());
			System.out.println("\nEtat des combattants de Joueur 2 : \n- " + p2_1.getNom() + " (en)=" + p2_1.getEnergie() + " (atk)=" + p2_1.getPuissance() + "\n- " + p2_2.getNom() + " (en)=" + p2_2.getEnergie() + " (atk)=" + p2_2.getPuissance());
			
			String choix1, choix2;	// Declaration de la chaine de caract�re choix1 pour "Attaquer ou manger" et choix2 pour le choix des pokemons
			
			if (joueur == 0) {
				/*----------------------------------------------------------------------------------------Les deux pokemon sont en vies-----------------Joueur 1-----------------------*/
				System.out.println("\n---------------Joueur 1---------------");
				
				if (p1_1.isAlive() && p1_2.isAlive()) {
					if (numPokemonJ1 == 0) { // On alterne entre les deux pokemons a chaque tours grace a numPokemonJ1
						System.out.println("Attaquer ou Manger [A / M]");
						choix1 = sc.nextLine(); // Saisie au clavier grace a sc.nextLine
						switch(choix1) { // On saisie d'attaquer ou de manger avec la saisie de la lettre A ou M
						case ("A"):
							System.out.println(p2_1.getNom() + "(a) ou " + p2_2.getNom() + "(b)");
							choix2 = sc.nextLine();
							switch(choix2){ // Si on choisie d'attaquer, on demande de choisir quel pokemon adverse on souhaite attaquer
							case ("a"):
								p1_1.attaquer(p2_1);
								break;
							case ("b"):
								p1_1.attaquer(p2_2);
								break;
							}
							break;
						case ("M"):
							p1_1.manger();
							break;
						}
					} else { // Meme chose que pour le premier pokemon.
						System.out.println("Attaquer ou Manger [A / M]");
						choix1 = sc.nextLine();
						switch(choix1) {
						case ("A"):
							System.out.println(p2_1.getNom() + "(a) ou " + p2_2.getNom() + "(b)");
							choix2 = sc.nextLine();
							switch(choix2){
							case ("a"):
								p1_2.attaquer(p2_1);
								break;
							case ("b"):
								p1_2.attaquer(p2_2);
								break;
							}
							break;
						case ("M"):
							p1_2.manger();
							break;
						}
					}
				/*----------------------------------------------------------------------------------------Pokemon 1 est en vies----------------------------------------*/
				} else if (p1_1.isAlive() == true && p1_2.isAlive() == false) {
					System.out.println("Attaquer ou Manger [A / M]");
					choix1 = sc.nextLine();
					switch(choix1) {
					case ("A"):
						System.out.println(p2_1.getNom() + "(a) ou " + p2_2.getNom() + "(b)");
						choix2 = sc.nextLine();
						switch(choix2){
						case ("a"):
							p1_1.attaquer(p2_1);
							break;
						case ("b"):
							p1_1.attaquer(p2_2);
							break;
						}
						break;
					case ("M"):
						p1_1.manger();
						break;
					}
				
				/*----------------------------------------------------------------------------------------Pokemon 2 est en vies----------------------------------------*/
				} else if (p1_2.isAlive() == true && p1_1.isAlive() == false) {
					System.out.println("Attaquer ou Manger [A / M]");
					choix1 = sc.nextLine();
					switch(sc.nextLine()) {
					case ("A"):
						System.out.println(p2_1.getNom() + "(a) ou " + p2_2.getNom() + "(b)");
						choix2 = sc.nextLine();
						switch(choix2) {
						case ("a"):
							p1_2.attaquer(p2_1);
							break;
						case ("b"):
							p1_2.attaquer(p2_2);
							break;
						}
						break;
					case ("M"):
						p1_1.manger();
						break;
					}
				}
				joueur = 1;
			} else{
				/*----------------------------------------------------------------------------------------Les deux pokemon sont en vies-----------------Joueur 2-----------------------*/
				System.out.println("\n---------------Joueur 2---------------");
				
				if (p2_1.isAlive() && p2_2.isAlive()) {
					if (numPokemonJ2 == 0) {
						System.out.println("Attaquer ou Manger [A / M]");
						choix1 = sc.nextLine();
						switch(choix1) {
						case ("A"):
							System.out.println(p1_1.getNom() + "(a) ou " + p1_2.getNom() + "(b)");
							choix2 = sc.nextLine();
							switch(choix2){
							case ("a"):
								p2_1.attaquer(p1_1);
								break;
							case ("b"):
								p2_1.attaquer(p1_2);
								break;
							}
							break;
						case ("M"):
							p2_1.manger();
							break;
						}
					} else {
						System.out.println("Attaquer ou Manger [A / M]");
						choix1 = sc.nextLine();
						switch(choix1) {
						case ("A"):
							System.out.println(p1_1.getNom() + "(a) ou " + p1_2.getNom() + "(b)");
							choix2 = sc.nextLine();
							switch(choix2){
							case ("a"):
								p2_2.attaquer(p1_1);
								break;
							case ("b"):
								p2_2.attaquer(p1_2);
								break;
							}
							break;
						case ("M"):
							p2_2.manger();
							break;
						}
					}
				/*----------------------------------------------------------------------------------------Pokemon 1 est en vies----------------------------------------*/
				} else if (p2_1.isAlive() == true && p2_2.isAlive() == false) {
					System.out.println("Attaquer ou Manger [A / M]");
					choix1 = sc.nextLine();
					switch(choix1) {
					case ("A"):
						System.out.println(p1_1.getNom() + "(a) ou " + p1_2.getNom() + "(b)");
						choix2 = sc.nextLine();
						switch(choix2){
						case ("a"):
							p2_1.attaquer(p1_1);
							break;
						case ("b"):
							p2_1.attaquer(p1_2);
							break;
						}
						break;
					case ("M"):
						p2_1.manger();
						break;
					}
				/*----------------------------------------------------------------------------------------Pokemon 2 est en vies----------------------------------------*/
				} else if (p2_2.isAlive() == true && p2_1.isAlive() == false) {
					System.out.println("Attaquer ou Manger [A / M]");
					choix1 = sc.nextLine();
					switch(choix1) {
					case ("A"):
						System.out.println(p1_1.getNom() + "(a) ou " + p1_2.getNom() + "(b)");
						choix2 = sc.nextLine();
						switch(choix2){
						case ("a"):
							p2_2.attaquer(p1_1);
							break;
						case ("b"):
							p2_2.attaquer(p1_2);
							break;
						}
						break;
					case ("M"):
						p2_2.manger();
						break;
					}
				}
				joueur = 0;
			}
		}
		System.out.println("---------------");
		
		// Quand les deux pokemons d'une equipe meurt, on affiche l'equipe gagnante
		if ((p1_1.getEnergie() + p1_2.getEnergie()) > (p2_1.getEnergie() + p2_2.getEnergie())) {
			System.out.println(p1_1.getNom() + " et " + p1_2.getNom() + " gagne en " + rounds + " rounds");
		} else {
			System.out.println(p2_1.getNom() + " et " + p2_2.getNom() + " gagne en " + rounds + " rounds");
		}
		
	}
}
