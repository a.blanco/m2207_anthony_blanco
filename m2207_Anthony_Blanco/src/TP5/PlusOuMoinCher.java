package TP5;

	import javax.swing.JFrame; // Librairies
	import javax.swing.JButton;
	import javax.swing.JLabel;
	import javax.swing.JTextField;
	
	import java.awt.Container;
	import java.awt.GridLayout;
	
	import java.awt.event.*;

	public class PlusOuMoinCher extends JFrame implements ActionListener{
		// Attributs
		Container panel = getContentPane(); // Layout
		
		int nbAlea;
		int nbRestant;
		
		JLabel lb1 = new JLabel("Votre proposition : "); // Labels, Bouton et TextField
		JLabel lb2 = new JLabel("Entr� un nombre. Essai Restant : 8");
		JTextField tf1 = new JTextField();
		JButton bt1 = new JButton("Verifier");
		
		// Constructeurs
		public PlusOuMoinCher() {
			super();
			this.setSize(450, 150); // Taille de l'application : 400x150
			this.setTitle("PlusOuMoinCher"); // Titre
			this.setLocation(20, 20);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setVisible(true);
			
			this.init(); // Initialise un nombre aleatoire
			
			panel.setLayout(new GridLayout(2,2));
			
			panel.add(lb1); // Label 1 : "Votre proposition : "
			panel.add(tf1); // TextField
			panel.add(bt1); // Ajout du bouton verifier
			panel.add(lb2); // Label 2 : "Entr� un nombre"
			
			bt1.addActionListener(this); // En attente d'action
		}

		// Methodes
		public void actionPerformed(ActionEvent e) { // Lorsque il y a une action, on utilise cette methode
			System.out.print("Action D�t�ct� : "); 
			
			int saisie = Integer.parseInt(tf1.getText()); // Conversion d'un string en entier int
			if (this.nbRestant == 0) {
				System.out.println("Defaite"); // Message console
				lb2.setText("Vous avez perdu !"); // Message graphique
				this.init();
			} else if (saisie < nbAlea) {
				System.out.println("+"); // Message console
				lb2.setText("Plus Grand ! Essai Restant : " + this.nbRestant); // Message graphique
				this.nbRestant--;
			} else if (saisie > nbAlea) {
				System.out.println("-"); // Message console
				lb2.setText("Plus Petit ! Essai Restant : " + this.nbRestant); // Message graphique
				this.nbRestant--;
			} else {
				System.out.println("GG"); // Message console
				lb2.setText("Bien Jou� ! Reussi en " + (8-this.nbRestant) + " coups"); // Message graphique
				this.init();
			}	
		}
		public void init() {
			this.nbAlea = (int)(1 - Math.random() * (1 - 100) + 1); // Cree un nouveau nombre aleatoire lors
			this.nbRestant = 8;
		}
		
		// Accesseurs
		/*Get*/
		
		
		/*Set*/
		
		
		// Methode Main
		public static void main (String[] args) {
			PlusOuMoinCher app = new PlusOuMoinCher();
		}
	}