package TP5;

import javax.swing.JFrame; // Librairies
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.Container;
import java.awt.FlowLayout;

import java.awt.event.*;

public class CompteurDeClick extends JFrame implements ActionListener{
	// Attributs
	int nbCliques = 0;
	
	Container panel = getContentPane(); // Layout
	JLabel lb1 = new JLabel("Vous avez cliqu� " + this.nbCliques + " fois"); // bouton et label
	JButton bt1 = new JButton("Click !");
	
	
	// Constructeurs
	public CompteurDeClick() {
		super();
		this.setSize(200, 100); // Taille de l'application : 200x100
		this.setTitle("CompteurDeClic"); // Titre
		this.setLocation(20, 20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		panel.setLayout(new FlowLayout());
		
		panel.add(bt1); // Ajout du bouton au panel
		bt1.addActionListener(this); // On "ecoute" le bouton pour soivoir si il y a un evenement ou non
		panel.add(lb1); // Ajout du label au panel
	}
	
	// Methodes
	public void actionPerformed(ActionEvent e) { // Lorsque il y a une action, on utilise cette methode
		System.out.println("Action D�t�ct�"); // Message console
		this.nbCliques++; // Incrementation du nb de clique
		lb1.setText("Vous avez cliqu� " + this.nbCliques + " fois"); // Modification du label
	}
	
	// Accesseurs
	/*Get*/
	
	
	/*Set*/
	
	
	// Methode Main
	public static void main (String[] args) {
		CompteurDeClick app = new CompteurDeClick();
	}
}
