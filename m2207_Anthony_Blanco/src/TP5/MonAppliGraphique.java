package TP5;
import javax.swing.JFrame;	// Import des package

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JButton; // Import des packages "composants"
import javax.swing.JLabel;
import javax.swing.JTextField;


public class MonAppliGraphique extends JFrame{
	// Attributs
	Container panel = getContentPane();	// Creation d'un panneau pour positionner les boutons
	
	JButton bt1 = new JButton("Bouton 1"); // Creation de nouveaux boutons
	JButton bt2 = new JButton("Bouton 2");
	JButton bt3 = new JButton("Bouton 3");
	JButton bt4 = new JButton("Bouton 4");
	JButton bt5 = new JButton("Bouton 5");
	
	JLabel monLabel = new JLabel("Je suis un label"); // Creation du label et du textfield
	JTextField monTextField = new JTextField("Je suis un text field");
	
	// Constructeurs
	public MonAppliGraphique() {
		super();	// Appel du constructeur de JFrame
		
		this.setTitle("Ma Premi�re Appli");	// Titre de l'appli
		this.setSize(500, 500);	// Taille de l'appli
		this.setLocation(20, 20); // Position a partir d'en haut a gauche
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Fonction du bouton "ferm�" lors du click
		this.setVisible(true);
		
		panel.setLayout(new GridLayout(3, 2));
		
		panel.add(bt1); // Ajout des boutons dans le panneau
		panel.add(bt2);
		panel.add(bt3);
		panel.add(bt4);
		panel.add(bt5);
		
		System.out.println("L'application a �t� cr��e !"); // Message console
	}
	
	// Methodes
	
	
	//Accesseurs
	/*Get*/
	
	
	/*Set*/
	
	
}
