package TP6;

import java.awt.GridLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.crypto.Cipher;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.io.PrintWriter;
import java.net.Socket;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

public class PanneauClient extends JFrame implements ActionListener{
	// Attributs
	Socket monSocket; // D�claration des l'objets
	PrintWriter monPrintWriter;
	
	Container panel = getContentPane();
	
	JButton bt_send = new JButton("Envoyer");
	
	JTextField textField = new JTextField();
	JLabel label = new JLabel();
	
	// Constructeur
	public PanneauClient(){
		super(); // Appel du constructeur de JFrame
		this.setTitle("Client - Panneau d'affichage");	// Titre de l'appli
		this.setSize(300, 120);	// Taille de l'appli
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Fonction du bouton "ferm�" lors du click
		this.setVisible(true);
		
		panel.setLayout(new GridLayout(3, 1));
		
		panel.add(textField); // Ajout des composants dans le panel
		panel.add(bt_send);
		panel.add(label);
		
		bt_send.addActionListener(this); // Ecoute du bouton
		
		this.connection();
	}
	
	// Methodes
	public void actionPerformed(ActionEvent e) { // Lorsque il y a une action, on utilise cette methode
		this.emettre();
	}
	public void emettre() {
		try {
			monPrintWriter = new PrintWriter(monSocket.getOutputStream()); // Instanciation de monPrinterWriter pour l'envoyer des donn�es
			
			monPrintWriter.println(textField.getText()); // Envoi du message
			label.setText("Envoi en cour . . .");	// Message informatif
			System.out.println("Envoi d'un message");
			Thread.sleep(500);
			
			monPrintWriter.flush(); // Permet de vider les buffers d'�criture
		}
		catch(Exception e) {
			label.setText("Echec de l'envoi");
			this.connection();
		}
	}
	public void connection() {
		try {
			label.setText("Connection en cour ...");
			monSocket = new Socket("localhost", 8888); // Instanciation du Socket pour etablir une connection a LocalHost sur le port 8888
			label.setText("Connect� !");
		}
		catch(Exception e){	// Si il y a une erreur, on retente la connection toutes les 3 secondes
			int times = 3;
			label.setText("Erreur lors de la connection");
			label.setText("Nouvelle tentative dans : " + times);
			try {
				while (times != 0) {
					label.setText("Nouvelle tentative dans : " + times);
					times--;
					Thread.sleep(1000);
					if (times == 0) {
						this.connection();
					}
				}
			}
			catch(Exception f) {
				label.setText("Fatal Erreur");
			}
		}
	}
	public static KeyPair buildKeyPair() throws NoSuchAlgorithmException {
        final int keySize = 2048;
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(keySize);      
        return keyPairGenerator.genKeyPair();
    }
	public static byte[] encrypt(PrivateKey privateKey, String message) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);  

        return cipher.doFinal(message.getBytes());  
    }
	
	public static void main(String[] args) {
		PanneauClient client = new PanneauClient();
	}
}
