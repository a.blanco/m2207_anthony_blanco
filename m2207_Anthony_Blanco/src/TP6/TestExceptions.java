package TP6;

public class TestExceptions {
	public static void main(String[] args) { // Classe main
		int x = 2;	// D�claration et Initialisation de x et y
		int y = 0;
		try { // Fonction Try{}Catch(){}
			System.out.println("y/x = " + y/x); // Test 1
			System.out.println("x/y = " + x/y); // Test 2
			System.out.println("Commande de fermeture du programme");
		}
		catch(Exception e){
			System.out.println("Une exception a �t� captur�e"); // Affiche la fin du programme
		}
		System.out.println("Fin du programme"); // Affiche la fin du programme
	}
}

// Lors de l'execution, le premiers calcule ce r�alise et s'affiche sans erreur alors que le deuxi�me n'apparait pas et cr�e un exception

/* Resultat :
 * 
 * 	 y/x = 0
 *	 Une exception a �t� captur�e
 *	 Fin du programme
 * 
 */