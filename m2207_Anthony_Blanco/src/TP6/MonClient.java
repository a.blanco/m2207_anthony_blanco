package TP6;

// Libraries
import java.io.PrintWriter;
import java.net.Socket;

public class MonClient {
	public static void main(String[] args) {
		Socket monSocket; // D�claration de l'objet Socket et monPrintWriter
		PrintWriter monPrintWriter;
		
		try {
			monSocket = new Socket("localhost", 8888); // Instanciation du Socket pour etablir une connection a LocalHost sur le port 8888
			System.out.println("Client : " + monSocket);
			
			monPrintWriter = new PrintWriter(monSocket.getOutputStream()); // Instanciation de monPrinterWriter pour l'envoyer des donn�es
			System.out.println("Envoi du message : HelloWorld");
			
			monPrintWriter.println("HelloWorld"); // Envoi "HelloWorld" comme donn�e
			monPrintWriter.flush(); // Permet de vider les buffers d'�criture
			
			monSocket.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
