package TP6;

// Libraries
import java.net.ServerSocket;
import java.net.Socket; 
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MonServeur {
	public static void main(String[] args) {
		ServerSocket monServerSocket; // D�claration de l'objet monServerSocket
		Socket monSocketClient;
		BufferedReader monBufferedReader;
		
		try {
			monServerSocket = new ServerSocket(8888); // Instanciation de monServerSocket
			System.out.println("Serveur Socket: " + monServerSocket); // Affichage des informations du serveur
			
			monSocketClient = monServerSocket.accept(); // En attente de connection d'un client
			System.out.println("Le client s'est connect�"); // Affichage apr�s la connection d'un client
			
			monBufferedReader = new BufferedReader(new InputStreamReader(monSocketClient.getInputStream())); // Lecture de monSocketClient
			String message = monBufferedReader.readLine(); // On stock la saisie du client dans 'message'
			System.out.println("Message : " + message); // Affichage du message dans la console
			
			monSocketClient.close();
			monServerSocket.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
