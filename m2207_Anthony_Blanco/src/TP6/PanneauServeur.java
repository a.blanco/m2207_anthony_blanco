package TP6;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import java.net.ServerSocket;
import java.net.Socket; 
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PanneauServeur extends JFrame implements ActionListener{
	// Attributs
	Container panel = getContentPane();
	
	JButton bt_exit = new JButton("Exit");
	JTextArea textArea = new JTextArea();
	
	//Constructeur
	public PanneauServeur() {
		super(); // Appel du constructeur de JFrame
		this.setTitle("Serveur - Panneau d'affichage");	// Titre de l'appli
		this.setSize(400, 300);	// Taille de l'appli
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Fonction du bouton "ferm�" lors du click
		this.setVisible(true);
		
		panel.setLayout(new BorderLayout(2, 1));
		
		panel.add(textArea, BorderLayout.CENTER); // Ajout des composants dans le panel
		panel.add(bt_exit, BorderLayout.SOUTH); 
		bt_exit.addActionListener(this); // Ecoute du bouton
		
		textArea.append("Le Panneau est actif\n"); // Affichage du message dans le TextArea
		this.demarrer();
	}
	
	//Methodes
	public void actionPerformed(ActionEvent e) { // Lorsque il y a une action, on utilise cette methode
		System.exit(-1);
	}
	public void demarrer() {
		textArea.setText(" ");
		textArea.append("Serveur en cour de d�marrage . . .\n");
		try {
			Thread.sleep(1000);
			textArea.append("Serveur d�marr�\n");
		}
		catch(Exception e) {
			textArea.append("Erreur de Demarrage\n");
		}
		this.ecouter();
	}
	public void ecouter() {
		ServerSocket monServerSocket; // D�claration de l'objet monServerSocket
		Socket monSocketClient;
		BufferedReader monBufferedReader;
		
		textArea.append("Serveur en attente de connection . . .\n\n");
		try {
			monServerSocket = new ServerSocket(8888); // Instanciation de monServerSocket
			monSocketClient = monServerSocket.accept(); // En attente de connection d'un client
			textArea.append("Le client s'est connect�\n"); // Affichage apr�s la connection d'un client dans le textArea
			
			monBufferedReader = new BufferedReader(new InputStreamReader(monSocketClient.getInputStream())); // Lecture de monSocketClient
			
			String message; // On stock la saisie du client
			while((message = monBufferedReader.readLine()) != null) {
				textArea.append("Message : " + message + "\n"); // Affichage du message dans le TextArea
			}
			
			monSocketClient.close();
			monServerSocket.close();
		}
		catch(Exception e) {
			textArea.setText("Erreur dans le traitement de la connection");
		}
	}
	
	public static void main(String[] args) {
		PanneauServeur Serveur = new PanneauServeur();
	}
}
