package TP2;


public class TestForme {

	public static void main(String[] args) {

		// La classe Forme nous renvoie la valeur du compteur qui est static
		System.out.println(Forme.getCompteur() + " objets de type Forme ont d�j� �t� cr��s.");
		
		//Exercice 1
		System.out.println("---------------Exercice 1---------------");
		
		// Instanciation de deux objets f1 et f2 de type forme
		Forme f1 = new Forme();	
		Forme f2 = new Forme("Vert", false);
		
		// Affichage de la couleur et du coloriage des objets f1 et f2
		System.out.println("Forme f1 : " + f1.getCouleur() + " - " + f1.isColoriage());
		System.out.println("Forme f2 : " + f2.getCouleur() + " - " + f2.isColoriage() + "\n");
		
		// Modification de l'objet f1 a ("Rouge" et true)
		f1.setCouleur("Rouge");
		f1.setColoriage(true);
		 
		// Nouvelle affichage de la couleur et du coloriage de f1 et f2
		System.out.println("Forme f1 : " + f1.getCouleur() + " - " + f1.isColoriage());
		System.out.println("Forme f2 : " + f2.getCouleur() + " - " + f2.isColoriage() + "\n");
		
		// "Demande" a f1 de ce decrire e faisant appel � la methode seDecrire
		System.out.println(f1.seDecrire());
		
		//Exercice 2
		System.out.println("\n---------------Exercice 2---------------");
		
		// Instanciation d'un objet cercle
		Cercle c1 = new Cercle();
		
		// Affichage de son rayon, de ca couleur, et de son coloriage
		System.out.println("un cercle de rayon " + c1.getRayon() + " est issue d'une forme de couleur " + c1.getCouleur() + " et de coloriage " + c1.isColoriage());
		
		// Appel de la methode seDecrire de la class Forme qu'il herite.
		System.out.println(c1.seDecrire() + "\n");
		
		// Instanciation d'un Cercle avec 1 parametre de rayon 2.5
		Cercle c2 = new Cercle(2.5);
		System.out.println(c2.seDecrire() + "\n");
		
		// Instanciation d'un Cercle avec 3 parametres de rayon 3.2, de couleur "jaune" et de coloriage false
		Cercle c3 = new Cercle(3.2, "Jaune", false);
		System.out.println(c3.seDecrire() + "\n");
		
		// Affichage de l'aire et le perimetre des cercles en appelant les methodes calculerAire et calculerPerimetre
		System.out.println("Cercle 3 : Aire = " + c2.calculerAire() + ", Perimetre = " + c2.calculerPermimetre());
		System.out.println("Cercle 3 : Aire = " + c3.calculerAire() + ", Perimetre = " + c3.calculerPermimetre());
		
		//Exercice 3
		System.out.println("\n---------------Exercice 3---------------");
		
		// Instanciation d'un cylindre
		Cylindre cy1 = new Cylindre();
		
		// Appel de la methode seDecrire
		System.out.println(cy1.seDecrire());
		
		// Creation d'un cylindre avec 4 parametres
		Cylindre cy2 = new Cylindre(4.2, 1.3, "Bleu", true);
		
		// Nouvelle description
		System.out.println(cy2.seDecrire());
		
		// Affichage du volume du cylindre a l'aide de la methode calculerVolume
		System.out.println("L'aire du cylindre est de " + cy1.calculerVolume(cy1) + " m^3");
		System.out.println("L'aire du cylindre est de " + cy2.calculerVolume(cy2) + " m^3");
		
		//Exercice 4
		System.out.println("\n---------------Exercice 4---------------");
		
		System.out.println(Forme.getCompteur() + " objets de type Forme ont d�j� �t� cr��s.");
		
	}
}
