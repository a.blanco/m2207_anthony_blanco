package TP2;

public class Cercle extends Forme{
	// Attributs
	private double rayon;
	
	// Constructeur
	public Cercle(double r, String couleur, boolean coloriage) {
		super(couleur, coloriage); // Appel du constructeur a 2 arguments
		this.rayon = r;
	}
	public Cercle(double r) {
		super(); // Appel le constructeur par default de la classe "m�re" 
		this.rayon = r;
	}
	public Cercle() { // Constructeur par default
		super(); // Appel le constructeur par default de la classe "m�re" 
		this.rayon = 1.0;
	}
	 
	// Methodes
	public String seDecrire() {
		// Retourne le rayon et fait appel a la description de la classe "m�re" grace a super.<methode>()
		return ("un cercle de rayon " + this.rayon + " est issue d'" + super.seDecrire());
	}
	public double calculerAire() {
		double a; // Variable local qui prendra la valeur de l'aire
		a = Math.PI * (this.rayon * this.rayon); // Calcule de l'aire
		return a;
	}
	public double calculerPermimetre() {
		double p; // Variable local qui prendra la valeur du perimetre
		p = 2 * Math.PI *this.rayon; // Calcule du perimetre
		return p;
	}
	
	// Accesseurs
	/*Set*/
	public void setRayon(double r) {
		this.rayon = r;
	}
	
	/*Get*/
	public double getRayon() {
		return this.rayon;
	}
	
}
