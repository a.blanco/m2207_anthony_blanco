package TP2;

public class Forme {
	// Attribut
	private String couleur;
	private boolean coloriage;
	public static int compteur;
	
	// Constructeur
	public Forme(String c, boolean r) {
		this.couleur = c;
		this.coloriage = r;
		compteur++;
	}
	public Forme() { // Constructeur par default
		this.couleur = "Orange";
		this.coloriage = true;
		compteur++;
	}
	 
	// Methodes
	public String seDecrire() {
		// Retourne la couleur et le coloriage
		return ("une forme de couleur " + this.couleur + " et de coloriage " + this.coloriage);
	}
	
	// Accesseurs
	/*Set*/
	public void setCouleur(String c) { // Initialisation de la couleur
		this.couleur = c;
	}
	public void setColoriage(boolean b) { // Initialisation du coloriage
		this.coloriage = b;
	}
	
	/*Get*/
	public String getCouleur() { // Retourne la couleur
		return (couleur);
	}
	public boolean isColoriage() { // Retourne le coloriage
		return (coloriage);
	}
	public static int getCompteur() { // Methode static permettant de retourner la valeur du compteur
		return (compteur);
	}
	
}
