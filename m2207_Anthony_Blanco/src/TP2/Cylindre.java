package TP2;

public class Cylindre extends Cercle{
	// Attributs
	private double hauteur;
	
	// Constructeur
	public Cylindre(double hauteur, double rayon, String couleur, boolean coloriage) {
		super(rayon, couleur, coloriage); // Appel de la fonction "m�re" (Cercle) qui fera lui m�me appel a sa classe "m�re" (Forme)
		this.hauteur = hauteur;
	}
	public Cylindre() {
		this.hauteur = 1.0;
	}
	
	// Methodes
	public String seDecrire() { // Retourne la hauteur et fait appel a la description de la classe "m�re" (Cercle)
		return ("un cylindre de hauteur " + this.getHauteur() + " est issue d'un cercle " + super.seDecrire());
	}
	public double calculerVolume(Cylindre cy1) { // Retourne le volume du cylindre
		double volume;
		volume = cy1.getHauteur() * cy1.calculerAire();
		return volume;
	}
	
	// Accesseurs
	/*Set*/
	public void setHauteur(double h) {
		this.hauteur = h;
	}
	
	/*Get*/
	public double getHauteur() {
		return (this.hauteur);
	}
	
}
