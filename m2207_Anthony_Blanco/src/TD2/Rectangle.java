package TD2;

public class Rectangle {
	// Attribut
	Point p1 = new Point();
	Point p2 = new Point();
	 
	// Constructeur 1
	public Rectangle(int x1, int y1, int x2, int y2) {
		this.p1.setX(x1);
		this.p1.setY(y1);
		this.p2.setX(x2);
		this.p2.setY(y2);
	}
	// Constructeur 2
	public Rectangle(Point p1, Point p2) {
		this.p1.setX(p1.getX());	// Recuperation des valeur de x et y de chaques Points
		this.p2.setX(p1.getY());
		this.p1.setY(p2.getX());
		this.p2.setY(p2.getY());	
	}
	
	// Methode
	public int getSurface() {
		int longX = this.p2.getX() - this.p1.getX();	// Calcule la difference entre les x et les y pour avoir des longueurs
		int longY = this.p2.getY() - this.p1.getY();
		int surface = longX * longY;	// Calcule de la surface
		return surface;
	}

}
