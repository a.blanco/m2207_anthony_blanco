package TD2;

public class Point {	// Déclaration de la class Point
	// Attributs
	static int compteur = 0;
	private int x, y;
	
	// Constructeur
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
		Point.compteur++;
	}
	// Deuxieme constructeur qui initialise x et y a 0
	public Point() {
		this.x = 0;
		this.y = 0;
		Point.compteur++;
	}
	
	//Methodes
	public int getX() {
		return this.x;	// Retourne la valeur de X
	}
	public int getY() {
		return this.y;	// Retourne la valeur de X
	}
	public void setX(int newX) {
		this.x = newX; 	// Initialise la valeur de X a la nouvelle valeur newX
	}
	public void setY(int newY) {
		this.y = newY;	// Initialise la valeur de Y a la nouvelle valeur newY
	}
	public String affiche() {	// Renvoye une chaine de caractères décrivant le point
		return ("Les coordonnées sont x = " + this.x + " et y = " + this.y);
	}
	public void resetCoordonnees() {	// Reinitialisation des coordonnées
		this.x = 0;
		this.y = 0;
	}
	void deplacer(int a, int b) {
		this.x += a;
		this.y += b;
	}
	public String distance(Point pt) {
		int distanceX;	// Declaration de X et Y pour stocker la distance entre les deux points
		int distanceY;
		
		// Test de X
		if (pt.getX() > this.x) {
			distanceX = pt.getX() - this.x;
		} else {
			distanceX = this.x - pt.getX();
		}
		// Test de Y
		if (pt.getY() > this.y) {
			distanceY = pt.getY() - this.y;
		} else {
			distanceY = this.y - pt.getY();
		}
		
		// Retourne la distance entre les x et les y
		return ("La distance est (x = " + distanceX + ", y = " + distanceY + ")");
	}
	static int getCpt() {
		return Point.compteur;
	}
	boolean equals(Point a) {
		int posX = a.getX();
		int posY = a.getY();
		
		if ((posX) == (this.x) && (posY) == (this.y)) {
			return true;
		}else {
			return false;
		}
	}
}
