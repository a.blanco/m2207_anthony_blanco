package TD2;

public class TestRectangle {	// Classe TestRectangle
	public static void main(String[] args) {
		
		// Instanciation d'un rectangle
		Rectangle rect, rect2;	// Declaration des attributs de type Rectangle et Point
		Point p1, p2;
		
		// Initialisation du Rectangle rect
		rect = new Rectangle(2,1,5,3);	
		System.out.println("La surface du rectangle est " + rect.getSurface()); // Affichage de ca surface
		
		// Initialisation de deux points qui formeront un rectangle
		p1 = new Point(2,1);
		p2 = new Point(5,2);
		rect2 = new Rectangle(p1,p2);	// Initialisation du Rectangle rect2
		System.out.println("La surface du rectangle est " + rect2.getSurface()); // Calcule l'aire du rectangle former par les points
	
		/* Resultat = 
		 * 
		 * 	 La surface du rectangle est 6
		 * 	 La surface du rectangle est 3
		 * 
		 * */
	}
}
