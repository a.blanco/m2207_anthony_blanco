package TD2;

public class TestPoint {	// Programme TestPoint
	public static void main(String[] args) {
		System.out.println(Point.compteur);
		Point p = new Point(2,7);	// Instanciation d'un nouveau point de coordonn�es (2,7)
		
		System.out.println("Valeur de X : " + p.getX());	// Affichage de x et y dans la console
		System.out.println("Valeur de Y : " + p.getY() + "\n");
		
		//System.out.println("Valeur de X : " + p.x);	// Affichage de x et y dans la console (Fonctionne pas)
		//System.out.println("Valeur de Y : " + p.y);
		
		Point p2 = new Point();	// Instanciation d'un nouveau point p2 sans coordonn�es
		
		// Affichage de p et p2 a l'aide de la methode affiche()
		System.out.println(p.affiche());
		System.out.println(p2.affiche() + "\n");
		
		// Reste p
		p.resetCoordonnees();
		
		System.out.println(p.affiche());
		System.out.println(p2.affiche() + "\n");
		
		p2.deplacer(5, 3);
		
		System.out.println(p.affiche());
		System.out.println(p2.affiche() + "\n");
		
		/* Resultat= Avant deplacement
		 * 
		 *	 Valeur de X : 2
		 *	 Valeur de Y : 7
		 *
		 *	 Les coordonn�es sont x = 2 et y = 7
		 *	 Les coordonn�es sont x = 0 et y = 0
		 *
		 *	 Les coordonn�es sont x = 0 et y = 0
		 *	 Les coordonn�es sont x = 0 et y = 0
		 *
		 *
		 * Resultat= Apr�s deplacement
		 * 
		 *	 Valeur de X : 2
		 *	 Valeur de Y : 7
		 *
		 *	 Les coordonn�es sont x = 2 et y = 7
		 *	 Les coordonn�es sont x = 0 et y = 0
		 *
		 *	 Les coordonn�es sont x = 0 et y = 0
		 *	Les coordonn�es sont x = 5 et y = 3
		 */
		
		Point p3 = new Point(3,4);
		Point p4 = new Point(6,0);
		
		System.out.println(p3.affiche());
		System.out.println(p4.affiche() + "\n");
		
		System.out.println(p3.distance(p4));
		
		/* Resultat= 
		 * 
		 * 	 Les coordonn�es sont x = 3 et y = 4
		 *	 Les coordonn�es sont x = 6 et y = 0
		 *
		 *	 La distance est (x = 3, y = 4)
		 *	 
		 */
		
		System.out.println("\nCompteur = " + Point.compteur);
		
		/* Resultat=
		 * 
		 * 	 Compteur = 0
		 * 
		 *	 [Code]
		 *
		 *	 Compteur = 4
		 *	 
		 */
		
		Point p5, p6, p7;
		p5 = new Point (2,3);
		p6 = new Point (2,3);
		p7 = p5;
		
		if (p5 == p6) {
			System.out.println("Ils sont �gaux !");
		}else {
			System.out.println("On ne peut pas tester l'�galit�\n");
		}
		
		/* Resultat = 
		 * 
		 * 	 On ne peut pas tester l'�galit�
		 * 
		 */
		
		System.out.println("p5 et p6 sont �gaux : " + p5.equals(p6));
		System.out.println("p5 et p7 sont �gaux : " + p5.equals(p7));
		
		/* Resultat =
		 * 
		 * p5 et p6 sont �gaux : true
		 * p5 et p7 sont �gaux : true
		 * 
		 * */
	}

}
