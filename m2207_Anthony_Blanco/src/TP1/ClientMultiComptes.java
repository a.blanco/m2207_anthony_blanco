package TP1;

public class ClientMultiComptes{
	// Attributs
		String nom, prenom;
		private Compte[] tabcompte = new Compte[10];
		static int nbCompte = 0;
	
	// Constructeur
	public ClientMultiComptes(String nom, String prenom, Compte compte) {
		this.nom = nom;
		this.prenom = prenom;
		tabcompte[nbCompte] = compte; // Stock le compte dans la premiere valeur du tableau lors de la creation de l'objet
		nbCompte++;
	}
	
	// Methodes
	public void ajouterCompte(Compte c) { // Ajout d'un compte dans le tableau
		tabcompte[nbCompte] = c;
		nbCompte++;
	}
	public void afficherEtatClient() { // Affiche le solde de tous les comptes du tableau
		double total = 0;
		
		System.out.println("Client : " + this.nom + " " + this.prenom);
		for (int i = 0; i < nbCompte; i++) { // Boucle for permettant de r�cup�rer le solde de chaque compte
			System.out.println("Compte n�" + i + " : Solde : " + tabcompte[i].getSolde() + "�");
			total += tabcompte[i].getSolde(); // Ajoute le solde de chaques comptes � la variable 'total'
		}
		System.out.println("Votre solde total est de : " + total + "�");
	}
	
	// Accesseur
	/*Set*/
	
	/*Get*/
	public double getSolde() { // Renvoie la somme du solde de tous les comptes
		double total = 0;
		
		for (int i = 0; i < nbCompte; i++) {
			total += tabcompte[i].getSolde();
		}
		return (total);
	}
}
