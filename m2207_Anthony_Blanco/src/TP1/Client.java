package TP1;

public class Client {
	// Attributs
	String nomClient;
	String prenomClient;
	private Compte compteCourant;
	
	// Constructeur
	public Client(String nom, String prenom, Compte compte) {
		this.nomClient = nom;
		this.prenomClient = prenom;
		this.compteCourant = compte;
	}
	
	//Methodes
	public void afficherSolde() {
		this.compteCourant.afficherSolde();
	}
	
	// Accesseurs
	/*Set*/
	
	
	/*Get*/
	public String getNom(){
		return (this.nomClient); // Retourne le nom du client
	}
	public String getPrenom(){
		return (this.prenomClient); // Retourne le prenom du client
	}
}
