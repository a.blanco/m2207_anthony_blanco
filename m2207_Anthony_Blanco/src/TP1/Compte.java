package TP1;

public class Compte {
	// Attributs
	private int numeroCompte;
	private double soldeCompte = 0.0;
	private double decouvertCompte = 0.0;
	
	// Constructeur
	public Compte(int numero) {
		numeroCompte = numero;
	}

	// Methodes
	public void afficherSolde() {	// Afficher le solde du compte
		System.out.println("Solde : " + this.soldeCompte + " �");
	}
	public void depot(double montant) {	// Permet de deposer de l'argent sur le compte
		this.soldeCompte += montant;
	}
	public String retrait(double montant) { // Permet de retirer de l'argent du compte sans depasser le decouvert
		double montant_max = this.soldeCompte + this.decouvertCompte;
		
		if (montant <= montant_max) { // Test si le montant que l'on veut retirer et plus petit que le montant autoriser.
			this.soldeCompte -= montant;
			if (this.soldeCompte <= 0) { // Si le solde du compte et inferieur a 0 on enleve la partie negative � la variable decouvertCompte
				this.decouvertCompte -= (this.soldeCompte)*(-1);
			}
			return ("Retrait effectu�. Nouveau Solde : " + soldeCompte + " �"); // Si le montant est autoriser, on l'effectue et on retourne une chaine de caract�re (Pas d'erreur)
		} else {
			return ("Retrait refus�. Solde disponible : " + soldeCompte + " �"); // Si le montant et refuser, on ne fait aucun traitement et on retourne une chaine de caract�re (Erreur)
		}
	}
	public String virer(Compte destinataire, double montant) { // Permet d'effectuer un virement sur le compte d'un autre client
		if (this.soldeCompte > montant) { // Test pour savoir si on a assez d'argent pour effectuer le virement
			this.soldeCompte -= montant;
			destinataire.depot(montant); // Ajout du montant au destinataire
			return ("Virement effectu�.");
		} else {
			return ("Virement refus�.");
		}
	}
	
	// Accesseurs
	/*Set*/
	public void setDecouvert(double montant) {
		this.decouvertCompte = montant;
	}
	
	/*Get*/
	public double getDecouvert() {
		return (this.decouvertCompte);
	}
	public double getNumero() {
		return (this.numeroCompte);
	}
	public double getSolde() {
		return (this.soldeCompte);
	}
}
