package TP1;
/*-----------------------------------------------------------------Programme-TestClient------------------------------*/
public class TestClient {

	public static void main(String[] args) {
		// Test final des virements entre client
		Compte c1, c2;
		c1 = new Compte(1); // Creation des comptes c1 et c2
		c2 = new Compte(2);
		
		c1.depot(1000); // depot de 1000 � sur le compte c1
		
		// Affichage du solde des comptes
		System.out.println("Solde compte 1 : " + c1.getSolde() + "�");
		System.out.println("Solde compte 2 : " + c2.getSolde() + "�\n");
		
		// Le compte n�1 effectue un virement de 600� sur le compte c2
		System.out.println(c1.virer(c2, 600));
		
		// Affichage du solde des comptes
		System.out.println("Solde compte 1 : " + c1.getSolde() + "�");
		System.out.println("Solde compte 2 : " + c2.getSolde() + "�\n");
		
		// Le compte n�1 effectue un deuxi�me virement de 600� sur le compte c2
		System.out.println(c1.virer(c2, 600));
		
		// Affichage du solde des comptes
		System.out.println("Solde compte 1 : " + c1.getSolde() + "�");
		System.out.println("Solde compte 2 : " + c2.getSolde() + "�\n");
	}

}
