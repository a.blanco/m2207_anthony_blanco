package TP1;
/*-----------------------------------------------------------------Programme-MaBanque--------------------------------*/
public class MaBanque {
	
	public static void main(String[] args) {
		System.out.println("Le compte n�2"); 
		// Declaration et instanciation d'un compte
		Compte compte1 = new Compte(1);
		
		// Premiere affichage du Decouvert
		System.out.print("Decouvert maximum de : "); 
		System.out.println(compte1.getDecouvert());
		
		// Initialisation du Decouvert a 100�
		System.out.print("Decouvert maximum de : "); 
		compte1.setDecouvert(100);
		
		// Seconds affichage du Decouvert
		System.out.println(compte1.getDecouvert());
		
		//Affichage du Solde
		compte1.afficherSolde();
		
		// Creation du compte n�2
		System.out.println("\nLe compte n�2"); 
		Compte compte2 = new Compte(2);
		
		// Versement de 1000�
		compte2.depot(1000);
		
		// Affiche le solde du compte n�2
		compte2.afficherSolde();
		
		// Retrait de 600� et affichage
		System.out.println(compte2.retrait(600)); 
		
		// Retrait de 700�
		System.out.println(compte2.retrait(600)); 
		
		// Changement du Decouvert a 500�
		compte2.setDecouvert(500);
		
		// Nouveau retrait de 700�
		System.out.println(compte2.retrait(700));
		
		// Client
		System.out.println("\nClient n�1"); 
		
		// Creation d'un client qui sera propri�taire du compte numero 2 et affichage du solde
		Client client1 = new Client("Cossu", "Julien", compte2);
		client1.afficherSolde();
	}
	
	
}
