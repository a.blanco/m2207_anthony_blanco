package TP3;

public class Cowboy extends Humain{ // Classe cowboy héritant de la classe Humain
	// Attributs
	private int popularite;
	private String caractéristique;
	
	// Constructeurs
	public Cowboy(String nom) {
		super(nom, "whiskey"); // Fait appelle a la classe mere pour definir le nom et la boisson par default du cowboy
		this.popularite = 0;
		this.caractéristique = "vaillant";
	}
	public Cowboy(String nom, String boisson) {
		super(nom, boisson);
		this.popularite = 0;
		this.caractéristique = "vaillant";
	}
	
	// Methodes
	public void tire(Brigand brigand) {
		System.out.println("*Le " + this.caractéristique + " " + this.nom + " tire sur " + brigand.nom + " ! PAN !*");
		super.parler("Prend ça, voyou !"); // Fait parler le cowboy concerné
	}
	public void sauve(Dame dame) {
		dame.estLiberee(); // Methode permettant de liberer la dame
	}
	
	// Accesseurs
	/*Set*/
		
		
	/*Get*/
}
