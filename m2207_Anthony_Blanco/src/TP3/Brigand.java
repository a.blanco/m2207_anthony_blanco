package TP3;

public class Brigand extends Humain{ // Classe Brigand qui herite de la classe humain
	// Attributs
	private String look;
	private int nbDameKidnappe;
	private int recompense;
	private boolean prison;
	
	// Constructeurs
	public Brigand(String nom) { // Constructeur definissant le nom, la boisson, le look la recompence et le nombre de dame kidnapp�
		super(nom, "cognac");
		this.look = "m�chant";
		this.prison = false;
		this.recompense = 100;
		this.nbDameKidnappe = 0;
	}
	public Brigand(String nom, String boisson) {
		super(nom, boisson);
		this.look = "m�chant";
		this.prison = false;
		this.recompense = 100;
		this.nbDameKidnappe = 0;
	}
	
	// Methodes
	public int getRecompense() { // Retourne la valeur en � du brigant
		return (recompense);
	}
	public String quelEstTonNom() { // Redefinition de la methode quelEstTonNom. Il se presente differament des autres humains
		 return (this.nom + " le " + this.look);
	 }
	public void sePresenter() { // Presentation du brigand. Il ajoute en plus le nombre de dame qui a captur� et sa valeur
		super.sePresenter();
		super.parler("J'ai l'aire Mechant et j'ai enlev� " + this.nbDameKidnappe + " dames.");
		super.parler("Ma t�te est mise � prix, " + this.recompense + " $ !!");
	}
	public void enleve(Dame dame) { // Methode permettant au brigand de captur� une dame
		super.parler("Ah ah ! " + dame.quelEstTonNom() + ", tu es ma prisonni�re !");
		dame.priseEnOtage(); // Appel de la methode priseEnOtage qui changera le status de libert� de la dame
		this.nbDameKidnappe++; // Incrementation du nombre de dame captur� et sa valeur
		this.recompense += 100;
	}
	public void emprisonner(Sherif s) { // Le Brigand peut se faire emprisonner pas un sherif
		super.parler("Damned, je suis fait ! " + s.quelEstTonNom() + ", tu m�as eu !");
		this.prison = true;
	}
}
