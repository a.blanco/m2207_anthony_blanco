package TP3;

public class Cachot{
	//Attributs
	private Humain tabHumain[] = new Humain[10];;
	
	//Methodes
	public void mettreEnCellule(Humain b) { // Mise en cellule d'un humain
		System.out.println(b.quelEstTonNom() + " a �t� mis en cellule");
		int numHum = 0;
		for (Humain i : tabHumain) { // On test chaque case du tableau pour savoir laquelle est vide et la remplir avec l'humain que l'on souhaite mettre en cellule.
			if (i == null) {
				tabHumain[numHum] = b;
				break;
			}
			numHum++;
		}
	}
	public void sortirDeCellule(Humain b){
		System.out.println(b.quelEstTonNom() + " est sortie de sa cellule");
		int numHum = 0;
		for (Humain i : tabHumain) { // On test chaque case du tableau pour savoir quelle humain est celui qu'on veut le liberer. Quand on l'a trouv�, on le supprime de la prison (on vide la case avec null)
			if (b == i) {
				tabHumain[numHum] = null;
			}
		}
	}
	public void compterLesPrisonniers() {
		int nbPrisonnier = 0;
		for (Humain b : tabHumain) { // Pour chaque valeur du tableau trouv�, on incremente de 1 le nombre de prisonnier
			if (b != null) {
				nbPrisonnier++;
			}
		}
		System.out.println("Il y a " + nbPrisonnier + " brigand(s) dans la prison de Saint-Pierre"); // Affichage dans la console le nombre de prisonnier dans la prison
	}
	
	/*
	 * 
	 * Pour pouvoir mettre tous les types de personnes dans une cellule, on utilise la classe Humain dont tout le
	 * monde herite. 
	 * 
	 */
	
}
