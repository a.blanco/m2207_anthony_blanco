package TP3;

public class Dame extends Humain{ // Classe dame h�ritant de la classe Humain
	// Attributs
	private boolean libre;
	
	// Constructeurs
	public Dame(String nom) {
		super(nom);  // Defini le nom de la dame qui est un humain et son statut de libert�
		this.libre = true;
	}
	public Dame(String nom, String boisson) {
		super(nom, boisson);
		this.libre = true;
	}
	
	// Methodes
	public void priseEnOtage() {
		this.libre = false;	
		super.parler("Au secours !"); // Fait appel a la methode parler de la classe humain avec la fonction super()
	}
	public void estLiberee() {
		this.libre = true;
		super.parler("Merci Cowboy");
	}
	public String quelEstTonNom() { // Redefinition de la methode quelEstTonNom pour ajouter "miss"
		 return ("Miss " + this.nom);
	 }
	 public String quelleEstTaBoisson()  {
		 return (this.boissonFav);
	 }
	 public void sePresenter() { // Redefinition de la methode sePresenter et dit si elle a �t� kidnapp�e ou non.
		 super.sePresenter();
			if (this.libre == true) {
				super.parler("Actuellement, je suis libre");
			} else {
				super.parler("Actuellement, je suis kidnapp�e");
			}
	 }
}
