package TP3;

public class Humain {
	// Attributs
	protected String nom;
	protected String boissonFav;
	
	// Constucteurs
	public Humain(String nom) {
		this.nom = nom;
		this.boissonFav = "lait";
	}
	public Humain(String nom, String boisson) {
		this.nom = nom;
		this.boissonFav = boisson;
	}
	
	// Methodes
	 public String quelEstTonNom() { // Retourne la valeur du nom de l'humain
		 return (this.nom);
	 }
	 public String quelleEstTaBoisson()  { // Retourne la boisson favorite
		 return (this.boissonFav);
	 }
	 public void parler(String texte) {
		 System.out.println("("+ this.nom + ") - " + texte); // Phrase de presentation permettant d'avoir le nom de la personne qui parle.
	 }
	 public void sePresenter() {
		 parler("Bonjour, je suis " + quelEstTonNom() + " et ma boisson pr�f�r�e : " + quelleEstTaBoisson() + ".");	// Phrase de presentation
	 }
	 public void boire() {
		 parler("Ah ! un bon verre de " + quelleEstTaBoisson() + " ! GLOUPS !.");
	 }
	// Accesseurs
	/*Set*/
	
	
	/*Get*/
	
	
}
