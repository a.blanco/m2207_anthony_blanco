package TP3;

public class Histoire {

	public static void main(String[] args) {
		Humain h1;
		Dame d1;
		Brigand b1, b2;
		Cowboy c1, clint;
		Sherif s1;
		
		System.out.println("------------------Exercice 1------------------");
		h1 = new Humain("Anthony");
		h1.sePresenter();
		h1.boire();
		
		System.out.println("\n------------------Exercice 2------------------");
		d1 = new Dame("Clemence", "Eau de coco");
		d1.sePresenter();
		
		System.out.println("\n------------------Exercice 3------------------");
		b1 = new Brigand("Antoine", "bi�re");
		b1.sePresenter();
		
		System.out.println("\n------------------Exercice 4------------------");
		c1 = new Cowboy("Lorenzo");
		c1.sePresenter();
		
		System.out.println("\n------------------Exercice 7------------------");
		b1.enleve(d1);
		b1.sePresenter();
		d1.sePresenter();
		c1.tire(b1);
		c1.sauve(d1);
		d1.sePresenter();
		
		System.out.println("\n------------------Exercice 8------------------");
		s1 = new Sherif("Anthony", "Pokka");
		s1.sePresenter();
		s1.coffrer(b1);
		s1.sePresenter();
		
		//System.out.println("\n------------------Exercice 9------------------");
		clint = new Sherif("Clint");
		/*
		 * Pour pouvoir voir quelle sont les m�thodes disponibles, on tape la ligne de commande "clint." puis eclipse 
		 * affichera la liste. On remarque que les methodes disponible sont celles de la class brigand.
		 * On ne peut pas demander a clint de coffrer un brigand car il dispose uniquement les methode de sa classe et celle 
		 * de la classe m�re.
		 *
		 */
		
		System.out.println("\n------------------Exercice 10------------------");
		Prison p = new Prison();
		b2 = new Brigand("Julien", "Pokka");
		
		p.compterLesPrisonniers();
		p.mettreEnCellule(b1);
		p.mettreEnCellule(b2);
		p.compterLesPrisonniers();
		p.sortirDeCellule(b1);
		p.compterLesPrisonniers();
		
		/*
		 * Resultat :
		 * 	------------------Exercice 1------------------
		 *	(Anthony) - Bonjour, je suis Anthony et ma boisson pr�f�r�e : lait.
		 *	(Anthony) - Ah ! un bon verre de lait ! GLOUPS !.
		 *	------------------Exercice 2------------------
		 *	(Clemence) - Bonjour, je suis Miss Clemence et ma boisson pr�f�r�e : Eau de coco.
		 *	(Clemence) - Actuellement, je suis libre
		 *
		 *	------------------Exercice 3------------------
		 *	(Antoine) - Bonjour, je suis Antoine le m�chant et ma boisson pr�f�r�e : bi�re.
		 *	(Antoine) - J'ai l'aire Mechant et j'ai enlev� 0 dames.
		 *	(Antoine) - Ma t�te est mise � prix, 100 $ !!
		 *	
		 *	------------------Exercice 4------------------
		 *	(Lorenzo) - Bonjour, je suis Lorenzo et ma boisson pr�f�r�e : whiskey.
		 *
		 *	------------------Exercice 7------------------
		 *	(Antoine) - Ah ah ! Miss Clemence, tu es ma prisonni�re !
		 *	(Clemence) - Au secours !
		 *	(Antoine) - Bonjour, je suis Antoine le m�chant et ma boisson pr�f�r�e : bi�re.
		 *	(Antoine) - J'ai l'aire Mechant et j'ai enlev� 1 dames.
	 	 *	(Antoine) - Ma t�te est mise � prix, 200 $ !!
		 *	(Clemence) - Bonjour, je suis Miss Clemence et ma boisson pr�f�r�e : Eau de coco.
		 *	(Clemence) - Actuellement, je suis kidnapp�e
		 *	*Le vaillant Lorenzo tire sur Antoine ! PAN !*
		 *	(Lorenzo) - Prend �a, voyou !
		 *	(Clemence) - Merci Cowboy
		 *	(Clemence) - Bonjour, je suis Miss Clemence et ma boisson pr�f�r�e : Eau de coco.
		 *	(Clemence) - Actuellement, je suis libre
		 *
		 *	------------------Exercice 8------------------
		 *	(Anthony) - Bonjour, je suis Anthony et ma boisson pr�f�r�e : Pokka.
		 *	(Anthony) - J'ai d�ja captur� 0 brigand(s) depuis le debut de ma carri�re.
		 *	(Anthony) - Au nom de la loi, je vous arr�te, Antoine le m�chant !
		 *	(Antoine) - Damned, je suis fait ! Anthony, tu m�as eu !
		 *	(Anthony) - Bonjour, je suis Anthony et ma boisson pr�f�r�e : Pokka.
		 *	(Anthony) - J'ai d�ja captur� 1 brigand(s) depuis le debut de ma carri�re.
		 *
		 *	------------------Exercice 10------------------
		 *	Il y a 0 brigand(s) dans la prison de Saint-Pierre
		 *	Antoine le m�chant a �t� mis en cellule
		 *	Julien le m�chant a �t� mis en cellule
		 *	Il y a 2 brigand(s) dans la prison de Saint-Pierre
		 *	Antoine le m�chant est sortie de sa cellule
		 *	Il y a 1 brigand(s) dans la prison de Saint-Pierre
		 *	
		 */
		
	}
}
