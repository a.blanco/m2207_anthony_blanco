package TP3;
import java.util.Vector; // Importation de la classe vecteur

public class Prison {
	//Attributs
	Vector <Humain> listeDesPrisonnier; // Definition d'un vecteur pouvant contenir des objets de type humain.
	
	//constructeurs
	public Prison () {
		this.listeDesPrisonnier = new Vector <Humain>(); // Instanciation du vecteur qui contiendra des objets humains (Sherifs, Dames, Brigands, ..)
	}
	
	//Methodes
	public void mettreEnCellule(Humain h) { // Mise en cellule d'un humain
		System.out.println(h.quelEstTonNom() + " a �t� mis en cellule");
		this.listeDesPrisonnier.add(h);
	}
	public void sortirDeCellule(Humain h){ // Sortie de cellule d'un humain
		System.out.println(h.quelEstTonNom() + " est sortie de sa cellule");
		this.listeDesPrisonnier.remove(h);
	}
	public void compterLesPrisonniers() {
		int nbBrigand = this.listeDesPrisonnier.size(); // Recup�re le nombre d'humain present en prison
		System.out.println("Il y a " + nbBrigand + " brigand(s) dans la prison de Saint-Pierre");
	}
	
	/*
	 * 
	 * Dans cette prison on pourra y mettre que les brigands.
	 * 
	 */
	
}
