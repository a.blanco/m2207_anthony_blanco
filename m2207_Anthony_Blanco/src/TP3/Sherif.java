package TP3;

public class Sherif extends Cowboy{ // Classe Sherif qui herite de cowboy et humain
	//Attributs
	protected int nbBrigand; // Nombre de brigand captur�
	
	//Constructeurs
	public Sherif(String nom) { // Definition du nom avec super() et du nb de brigand qu'il a arr�t�
		super(nom);
		nbBrigand = 0;
	}
	public Sherif(String nom, String boisson) {
		super(nom, boisson);
		nbBrigand = 0;
	}
	
	
	// Methodes
	public void sePresenter() { // Lors de la presentation du sherif il ajoute le nombre de personne qu'il a arr�t�
		super.sePresenter();
		super.parler("J'ai d�ja captur� " + this.nbBrigand + " brigand(s) depuis le debut de ma carri�re.");
	}
	public void coffrer(Brigand b) { // Permet au sherif d'arr�ter un brigand et de l'emprisonner
		super.parler("Au nom de la loi, je vous arr�te, " + b.quelEstTonNom() + " !");
		b.emprisonner(this); // Change le statut de "prison" du brigand
		nbBrigand++; // Incrementation du nombre de brigand arr�t� de 1
	}
}
