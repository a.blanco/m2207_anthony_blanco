package TD3;

/* Lorsque l'on cree une class animal vide et que l'on cree un nouvelle "animal", un constructeur par default est 
 * cree lors de la compilation.
 * 
 */

public class Animal {
	// Attribut
	
	// Constructeur
	
	// Methodes
	public String affiche() {
		return ("Je suis un animal");
	}
	public String cri() {
		return ("...");
	}
	public final String origine() {
		return ("La classe Animal est la m�re de toutes les classes");
	}
}
