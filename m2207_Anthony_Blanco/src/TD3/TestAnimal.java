package TD3;

public class TestAnimal {

	public static void main(String[] args) {
		Animal animal = new Animal();
		Chien chien = new Chien();
		Chat chat = new Chat();
		
		System.out.println(animal.cri());
		System.out.println(chien.cri());
		System.out.println(chat.cri() + "\n");
		
		//System.out.println(animal.miauler());
		System.out.println(chat.miauler() + "\n");
		
	}
	
	/*
	 * Resultat=
	 * 		Je suis un chien
	 * 		Ouaf, Ouaf
	 * 
	 * 		Je suis un chat
     * 		Miaou, Miaou
     * 
     * 		La classe Animal est la m�re de toutes les classes
     * 		La classe Animal est la m�re de toutes les classes
     * 
     * Un class fill dispose des methodes de la class mere mais pas inversement
     * C'est pour cela qu'il il y a une erreur a 'animal.miauler()
	 * 
	 * */
}
