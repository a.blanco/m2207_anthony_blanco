package TD3;

// Creation de la class Chien heritant de la class Animal
public class Chien extends Animal{
	public String affiche() {
		return ("Je suis un chien");
	}
	public String cri() {
		return ("Ouaf, Ouaf");
	}
}
