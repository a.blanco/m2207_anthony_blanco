package TD1;


public class Exercice_6 {	/*-------------------------MesNotes-------------------------*/

	// Definition et initialisation du tableau de note
	public static int tabNotes[] = {11,12,2,5,18,14,15,8,13,15};
	
	
	public static void main(String[] args) {
		System.out.println("Moyenne : " + noteMoy());	// Appel de la methode noteMoy et affiche ce qu'il retourne
		System.out.println("Note Maximal : " + noteMax());	// Appel de la methode noteMax et affiche ce qu'il retourne
		System.out.println("Note Minimal : " + noteMin());	// Appel de la methode noteMin et affiche ce qu'il retourne
	}
	
	// Methodes
	public static double noteMoy() {	// Methode noteMoy qui calcule la moyenne des notes	
		int somme = 0;
		for (int i = 0; i < tabNotes.length; i++) {	// On ajoute chaque �l�ment du tableau a somme
			somme += tabNotes[i];
		}
		double moyenne = (double)somme/(tabNotes.length);	// On divise somme par le nobre d'�l�ment du tableau
		return moyenne;	// retoune la valeur noteMoy
	}
	public static int noteMax() {	// Methode noteMoy qui cherche "la note maximal du tableau
		int max = tabNotes[0];	
		for (int i : tabNotes) {	// Pour chaque valeur du tableau tabNotes j'enregistre la valeur la plus grand dans la variable max
			if (i > max) {
				max = i;
			}
		}
		return max;	// Retourne la valeur maximal
	}
	public static int noteMin() {	// Methode noteMoy qui cherche "la note maximal du tableau
		int min = tabNotes[0];
		for (int i : tabNotes) {	// Pour chaque valeur du tableau tabNotes j'enregistre la valeur la plus petit dans la variable min
			if (i < min) {
				min = i;
			}
		}
		return min;	// Retourne la valeur minimal
	}
}
	
