package TD1;

public class Exercice_5 {

	public static void main(String[] args) {
		// Declaration et initialisation du tableau
		int tab[] = {1,7,4,6};
		
		// Inverser les valeurs du tableau
		for (int i = 0; i < (tab.length/2); i++){
			int temp = tab[i];
			tab[i] = tab[tab.length - i - 1];
			tab[tab.length - i - 1] = temp;
		}
		
		// affichage du tableau
		for (int i = 0; i < (tab.length); i++){
			System.out.print(tab[i]);
		}
	}
}
