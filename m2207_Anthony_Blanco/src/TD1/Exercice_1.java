package TD1;

public class Exercice_1 { /*-------------------------Sant� de Bob-------------------------*/
	// Attribut
	String nomHumain;
	float poidsHumain;
	int ageHumain;
	float tailleHumain;
	
	int ageRetraite;
	double imc;
	
	// Constructeur
	public Exercice_1(String nom, int age, double poids, int taille){
		nomHumain = nom;
		poidsHumain = (float) poids;
		ageHumain = age;
		tailleHumain = taille;
	}
	
	// M�thodes
	public void info(){ // Affiche les informations de l'humain
		getIMC();
		ageRetraite = 65 - ageHumain;
		System.out.println(nomHumain + " � " + ageHumain + " ans, fait " + poidsHumain + " kg et lui reste " + ageRetraite + " ann�es avant la retraite.");
		System.out.print(nomHumain + " � un IMC de : " + (float)imc);
		if (imc < 25){
			System.out.println(" Il est Normal.");
		} else if ((imc >= 25) && (imc < 27)){
			System.out.println(" Il est en Surpoids L�ger.");
		} else {
			System.out.println(" Il est en Surpoids.");
		}
	}
	public void vieillit(float age){ // Fait vieillir l'humain
		while (ageHumain < age){
			poidsHumain += 1.2000;
			ageHumain++;
		}
	}
	public void vieilliera(float poids){ // Calcule approximativement le poids de l'humain dans le future
		int ageVieux = ageHumain;
		float poidsVieux = poidsHumain;
		while (poidsVieux < poids){
			poidsVieux += 1.2000;
			ageVieux++;
		}
		System.out.println("Il p�sera 70 kg � " + ageVieux + " ans");
	}
	
	public void doSport(float imcSouhaiter){ // Fait du sport pour perdre du poids
		int kilometre = 0;
		
		double grammeKilogramme = 0.024;
		double tailleHumain = (double)this.tailleHumain;
		double poidsHumain = (double)this.poidsHumain;
		
		while (imc > imcSouhaiter){ // Boucle qui calcule le nombre de kilometre � parcourir en fonction de l'IMC voulu
			this.imc = poidsHumain / ((tailleHumain/100)*(tailleHumain/100));
			poidsHumain -= grammeKilogramme;
			kilometre++;
		}
		System.out.println("La distance � parcourir est " + kilometre + " kilometres pour avoir un IMC de " + imcSouhaiter);
	}
	
	public void getIMC(){ // R�cup�rer l'IMC de l'humain
		imc = this.poidsHumain / ((tailleHumain/100)*(tailleHumain/100));
	}
	
	public static void main(String[] args){ // Class main
		
		// Creation d'un humain nomm� Bob, qui � 18 ans, p�se 58 kg et mesure 182 cm.
		Exercice_1 bob = new Exercice_1("Bob", 18, 58, 182);
		
		bob.info(); // Affiche les informations de Bob
		bob.vieilliera(70); // Calcule qui "pr�dit" l'age de Bob � 70 kg
		System.out.println("");
		bob.vieillit(40); // Fait vieillir Bob jusqu'a 70 ans
		bob.info(); // Affiche les informations de Bob
		System.out.println("");
		bob.doSport(23); // Calcule du nombre de kilometre a parcourir pour avoir un IMC de 23.
	}
}
