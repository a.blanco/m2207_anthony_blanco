package TD1;

import java.util.Scanner;

public class Exercice_4 { /*-------------------------DevineLeNombre-------------------------*/

	public static void main(String[] args) {
		int random = (int)(Math.random() * 40)/2; // Declaration et initialisation d'un nombre aleatoire compris entre 0 et 20
		int nbChoisie;
		int nbEssai = 1;
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);	// Creation de l'objet scanner permettant la saisie au clavier
		
		System.out.print("Saisissez un nombre compris entre 0 et 20 : ");
		
		do {	// Debut de la boucle doWhile
			nbChoisie = sc.nextInt();	// Demande de saisie d'un nombre entier
		
			if (nbChoisie > 20) {
				System.out.println("Veuillez saisir un nombre compris entre 0 et 20.\n");
				nbEssai--; // Ne compte pas dans le nombre d'essai
			} else if (nbChoisie > random) {	// Test du nombre saisie par le joueur
				System.out.print("Plus Petit : ");
			} else if (nbChoisie < random) {
				System.out.print("Plus Grand : ");
			} else {
				System.out.println("Vous avez r�ussi en " + nbEssai + " coups");
			}
			nbEssai++;
		} while (nbChoisie != random); // Fin de la boucle
	}
}
