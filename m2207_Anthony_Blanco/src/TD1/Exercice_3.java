package TD1;

import java.util.*;

public class Exercice_3 { /*-------------------------SaisieClavier-------------------------*//**/

	public static void main(String[] args) {

		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);	// Creation de l'objet scanner 
		sc.useLocale(Locale.US);	// Definition du parse local pour utiliser un point au lieu d'une virgule lors de la saisie.
		
		System.out.print("Entre votre nom : ");
		String name = sc.nextLine(); // En attente de chaine de caract�re pour remplir la variable name.
		
		System.out.print("Entre un entier : ");
		int entier = sc.nextInt();	// En attente d'un entier pour remplir la variable entier.
		
		System.out.print("Entre un reel : ");
		double reel = sc.nextDouble();	// En attente d'un reel pour remplir la variable reel.
		
		float somme = (float) (entier + reel);	// Fait la somme de "entier" et "reel".
		
		// Message 
		System.out.println("Bonjour " + name + ", La somme de " + entier + " + " + reel + " est " + somme);
		
	}

}
